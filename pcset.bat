set path=%~D0\Experiment\Java\jdk1.8.0_40\bin;%~D0\xampplite\mysql\bin;%path%
set classpath=%~D0\Experiment\Java\jdk1.8.0_40\jre\lib\ext\spc101.jar;%~D0\Experiment\Java\jdk1.8.0_40\jre\lib\ext\mysql-connector-java-5.1.10-bin.jar;%classpath%

copy Experiment\jcpad_ini Tools\jcpad231\jcpad.ini

echo CompilerPath=%~D0\Experiment\Java\jdk1.8.0_40\bin\javac.exe >> Tools\jcpad231\jcpad.ini
echo HelpPath=%~D0\Tools\jcpad231\cpad_manual.html >> Tools\jcpad231\jcpad.ini
echo DataFolderPath=%~D0\JavaData >> Tools\jcpad231\jcpad.ini
echo BackupFolderPath=%~D0\Tools\jcpad231\Backup >> Tools\jcpad231\jcpad.ini

copy Experiment\my_ini xampplite\mysql\bin\my.ini
