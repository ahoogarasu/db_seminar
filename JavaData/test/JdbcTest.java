import java.sql.*;
class JdbcTest{
	public static void main(String[] args) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String strConn = "jdbc:mysql://localhost/testdb"
				+ "?user=testuser&password=";
			conn = DriverManager.getConnection(strConn);
			stmt = conn.createStatement();
			String strSql = "SELECT * FROM testtable";
			rs = stmt.executeQuery(strSql);
			while (rs.next()) {
				int intId = rs.getInt("id");
				String strName = rs.getString("name");
				int intValue = rs.getInt("value");
				System.out.println(intId + "," + strName + "," + intValue);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try{rs.close();} catch (SQLException e) {e.printStackTrace();}
			try{stmt.close();} catch (SQLException e) {e.printStackTrace();}
			try{conn.close();} catch (SQLException e) {e.printStackTrace();}
		}
	}
}
